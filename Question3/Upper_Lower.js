function upper_lower() {
    const sentence = document.getElementById('input').value;
    const capt_words = U_p_Value(sentence);
    console.log("result", capt_words);
    document.getElementById('result').innerHTML = capt_words.join(' ');
}

function U_p_Value(sentence) {
    let words = sentence.split(" ");
    console.log(words)
    let capt_words = [];
    for (let i = 0; i < words.length; i++) {
        const word = words[i];
        const capt_word = word.charAt(0).toUpperCase() + word.slice(1).toLowerCase();
        capt_words.push(capt_word);
    }
    return capt_words;
}

