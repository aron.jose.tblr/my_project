const express = require('express');
// const cors = require('cors');
var bodyParser = require('body-parser');

const PORT = process.env.PORT || 8090

// app.use(cors());
const app = express();
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(bodyParser.urlencoded({ extended: true }));


const roleRouters = require('./routes/roleRoutes.js')
app.use('/api/roles', roleRouters)

const userRouters = require('./routes/userRotes.js')
app.use('/api/user', userRouters)


app.listen(PORT, () => {
    console.log(`server is running port is ${PORT}`);
})