const user = require('../models/user');
let roles = require("../models/roles");
var jwt = require("jsonwebtoken");
const HashPassword = require("../helper/password").HashPassword;
const verifyHashPassword = require("../helper/password").verifyHashPassword

// ---------------------------------Create User -------------------------------------------------------
const addUser = async (req, res) => {
    try {
        const requiredFields = [
            "first_name",
            "email",
            "phone_number",
            "password"
        ];
        for (const field of requiredFields) {
            if (!req.body[field] || req.body[field].trim() === "") {
                return res.status(400).json({ error: `${field} is a required field` });
            }
        }
        const hashedPassword = HashPassword(req.body.password);
        const createdUser = {
            first_name: req.body.first_name,
            last_name: req.body.last_name,
            email: req.body.email,
            phone_number: req.body.phone_number,
            password: hashedPassword.hash,
            password_salt: hashedPassword.salt
        }
        const userdata = new user(createdUser)
        let saved = await userdata.save()
        if (saved) {
            res.status(200).json({ message: "success", saved: saved });
        } else {
            res.status(400).json({ Error: 'Error in inserting a new record' });
        }
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};

// ------------------------------------user list and Single User-------------------------------------------
const getUser = async (req, res) => {
    try {
        let r
        if (req.query.user_id) {
            r = await user.findById({ _id: req.query.user_id })
        } else {
            r = await user.find()
        }
        if (!r || r === "" || r === null) throw err
        console.log(r, "hellow");

        if (r) {
            res.status(200).json({ data: r })
        } else {
            res.status(400).error;
        }
    } catch (err) {
        console.log(err, "err")
        res.status(400).json({ error: "some error occured", err: err })
    }
}

// ------------------------------------------Update User Details ------------------------------------------
const putUser = async (req, res) => {
    try {
        const userDetails = {
            first_name: req.body.first_name,
            last_name: req.body.last_name,
            email: req.body.email,
            phone_number: req.body.phone_number
        };
        const updateUser = await user.findByIdAndUpdate(req.query.user_id, userDetails, { new: true });
        if (!updateUser) {
            return res.status(404).json({ error: 'User not found' });
        }
        res.json(updateUser);
    } catch (error) {
        res.status(500).json({ error: 'An error occurred while updating data.' });
    }
}

// -----------------------------------------------Delete User --------------------------------------------
const deleteUserById = async (req, res) => {
    try {
        const deleteUser = await user.findByIdAndRemove(req.query.user_id);
        if (deleteUser) {
            res.status(200).json({ success: "successFully Deleted" });
        } else {
            res.status(400).json({ Error: "User not Found!" })
        }

    } catch (err) {
        res.status(500).json({ err: "Some error occured!!" })
    }
}

// ----------------------------------------Login -----------------------------------------------------------

const login = async (req, res) => {
    try {
        const requiredFields = ["email", "password"];

        for (const field of requiredFields) {
            if (!req.body[field]) {
                return res.status(400).json({ message: `${field} is required`, code: 400 });
            }
        }

        const users = await user.findOne({ email: req.body.email });

    if (users) {
            const isPasswordValid = await verifyHashPassword(req.body.password, users.password_salt);

            if (isPasswordValid.hash === users.password) {
                const tokenPayload = {
                   _id: users._id,
                    firstname: users.first_name,
                    lastname: users.last_name,
                    email: users.email
                };

                const token = jwt.sign(tokenPayload, "secretkey", { expiresIn: 86400 });

                const userDetails = {
                    _id: users._id,
                    first_name: users.first_name,
                    last_name: users.last_name,
                    email: users.email,
                    role_id: users.role_id,
                    token: token
                };

                return res.status(200).json({ userDetails });
            } else {
                return res.status(401).json({ message: "Authentication failed" });
            }
        } else {
            return res.status(404).json({ message: "User does not exist" });
        }
    } catch (error) {
        return res.status(500).json({ message: "An error occurred" });
    }
};

module.exports =
    {
        addUser,
        getUser,
        putUser,
        deleteUserById,
        login
    };
