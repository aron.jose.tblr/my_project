const mongoose = require('mongoose');
const db = require('../config/dbConfig');
// const roles = require("../models/roles");

const Schema = mongoose.Schema;

const userSchema = new Schema({
    first_name: {
        type: String,
        required: true
    },
    last_name: String,
    email: {
        type: String,
        required: true,
        unique: true
    },
    phone_number: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    password_salt: {
        type: String,
        required: true
    },
    status: {
        type: String,
        required: true,
        enum: ['active', 'inactive', 'trash'],
        default: 'active'
    },
    role_id: {
        type: Schema.Types.ObjectId,
        default: new mongoose.Types.ObjectId("64d9bbc01c7194c84abd17bb"),
        ref: 'roles'
    },
    created_at: {
        type: Date,
        default: Date.now
    },
    updated_at: {
        type: Date,
        default: Date.now
    }

});

const User = db.model('users', userSchema);

module.exports = User;
