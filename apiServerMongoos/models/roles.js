const db = require('../config/dbConfig');

const schema = {
    role_name: String
};
const roles = db.model("roles", schema);

module.exports = roles