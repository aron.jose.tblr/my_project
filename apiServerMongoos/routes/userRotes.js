const userController = require('../controller/userController.js');
const express = require('express');
const router = express.Router();

router.post('/post', userController.addUser);
router.get('/get', userController.getUser);
router.put('/put', userController.putUser);
router.delete('/delete', userController.deleteUserById);
router.post('/login', userController.login);



module.exports = router