
function alphabet_order() {
    const str = document.getElementById('input').value;
    const result = getSortedValue(str)
    console.log("result", result)
    document.getElementById('result').innerHTML = result;
}
function getSortedValue(str) {
    let sorted = str.split('').sort().join('');
    console.log("sorted", sorted)
    return sorted;
}